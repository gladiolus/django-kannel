import logging
import urllib
import urllib2

from celery import shared_task
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils import timezone


logger = logging.getLogger(__name__)


@shared_task
def send_sms(sms_id):
    from sms.models import Sms
    sms = Sms.objects.get(pk=sms_id)

    delivery_uri = reverse('sms:delivery-report',
                           kwargs={'sms_id': sms.pk, 'status': 0})
    delivery_url = "{0}{1}%d".format(settings.KANNEL_CALLBACK_URL,
                                     delivery_uri[:-1])

    params = {
        'username': settings.KANNEL_USERNAME,
        'password': settings.KANNEL_PASSWORD,
        'from': sms.sender.encode('utf-8'),
        'to': sms.phone,
        'text': sms.text.encode('utf-8'),
        'charset': 'UTF-8',
        'coding': 2,
        'dlr-mask': 31,
        'dlr-url': delivery_url,
        'deferred': max(0, int((sms.time - timezone.now()).total_seconds() / 60)),
    }
    kannel_url = '{0}?{1}'.format(settings.KANNEL_URL, urllib.urlencode(params))

    try:
        f = urllib2.urlopen(kannel_url)
    except (urllib2.URLError, IOError) as e:
        logger.exception("Sms kannel request error: %s", e)
    else:
        f.close()
        sms.sent = True
        Sms.objects.filter(pk=sms.pk).update(sent=True)

    return sms
